<?php
session_start();
include "database.php";
$tbl_name = "usuario";

if (isset($_SESSION['nick'])) {

	if (isset($_SESSION['tiempo'])) {

		$inactivo = 300;
		$vida_session = time() - $_SESSION['tiempo'];

		if ($vida_session > $inactivo) {
			session_unset();
			session_destroy();
			header("Location: index.php");
			exit();
		} else {
			$_SESSION['tiempo'] = time();
		}
	} else {

		$_SESSION['tiempo'] = time();
	}

	$nick = $_SESSION['nick'];

	$sql = "Delete From $tbl_name Where nick='$nick'";


	if (mysqli_query($mysqli, $sql)) {
		?>
        <script language="javascript"> alert("Usuario eliminado correctamente");</script>

		<?php

		session_destroy();
		header('Location: index.php');
	} else {
		echo "Error: " . $sql . "<br>" . mysqli_error($mysqli);
		echo "Perfil NO eliminado";
		//header ('location: index.php');
	}

} // $_SESSION['nick']


if (isset($_SESSION['nombreAdmin'])) {

	$nombreAd = $_SESSION['nombreAdmin'];
	$contrasenaAd = $_SESSION['contrasenaAdmin'];

	$tbl_name = "administrador";

	$sql = "Delete From $tbl_name Where nombreAdmin = '$nombreAd'";


	if (mysqli_query($mysqli, $sql)) {

		session_destroy();
		header('location: adminIndex.php');
	} else {
		echo "Error: " . $sql . "<br>" . mysqli_error($mysqli);
		echo "Perfil NO eliminado";
		//header ('location: index.php');
	}
}

?>
