<!DOCTYPE html>
<html lang="en">

<head>
    <!-- <meta> <link> -->
	<?php include_once "meta&links.php" ?>
    <title>Login</title>

    <script>

		<?php session_start();
		if(isset($_SESSION['error']) && $_SESSION['error'] == "login"){ ?>
            alert("Datos incorrectos");
		<?php   } ?>

    </script>

</head>

<body>

<!-- Navigation -->
<?php include "headerSimple.php" ?>
<!-- Login -->

<div class="container">

    <h1><br> Iniciar Sesión <br></h1>

    <form action="checklogin.php" method="post">
        <label>Nick Usuario:</label><br>
        <input name="nick" id="nick" required="required" type="text"><br><br>
        <label>Password:</label><br>
        <input name="contrasena" id="contrasena" required="required" type="password"><br><br>
        <input name="submit" value="Login" type="submit"><br><br>

    </form>
    <p> ¿Aún no tienes cuenta? <a href=<?php echo "userRegister.php"; ?>>Accede al registro</a></p>

</div>

<!-- Footer -->
<?php include "footer.php" ?>

</body>

</html>
