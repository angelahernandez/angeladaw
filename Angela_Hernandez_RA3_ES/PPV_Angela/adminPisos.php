<?php session_start();

if (!isset($_SESSION['nombreAdmin'])) {	header('Location: administracion.php');}



?>

<html>
<head>
    <!-- <meta> <link> -->
	<?php include_once "meta&links.php" ?>

    <title>Admin - Pisos</title>

    <style> td:first-child { font-weight: bold;}  </style>

    <script>

		<?php
		if(isset($_SESSION['error'])){

	    	if($_SESSION['error'] == "nuevopiso"){    ?>
             alert("Error al añadir piso, prueba con otro titulo");
            <?php   }

	    	if($_SESSION['error'] == "eliminarpiso"){        ?>
                 alert("Error al elimar piso, introduce el titulo de un piso");
		    <?php }} ?>


        function validacion() {
            telefonoVendedor = document.getElementById("telefonoVendedor").value;
            if (telefonoVendedor.length != 9) {
                alert("Introduce  telefono de contacto válido");
                return false;
            }
            return true;
        }


        function validacionModificar() {
            pisoModificar = document.getElementById("pisoModificar").value;
            if (pisoModificar.length == 0) {
                alert("Introduce titulo del piso a modificar");
                return false;
            }
            return true;
        }
    </script>

</head>
<body style="background-color: #FFD7B2;">

<!-- Navigation --> <?php include "headerAdmin.php"; ?>

<div class="container">

    <div class="col-lg-9">
        <h4> Añadir Piso</h4>

        <form action="adminNuevoPiso.php" method="post" enctype="multipart/form-data" onsubmit="return validacion()">
            <label>Titulo:</label><br>
            <input name="titulo" id="titulo" required="required" type="text">
            <br>
            <label>m2:</label><br>
            <input name="m2" id="m2" required="required" type="text">
            <br>
            <label>Habitaciones:</label><br>
            <input name="nHabit" id="nHabit" required="required" type="text">
            <br>
            <label>Planta:</label><br>
            <input name="planta" id="planta" required="required" type="text">
            <br>
            <label>Precio:</label><br>
            <input name="precio" id="precio" required="required" type="text">
            <br>
            <label>Distancia al Montessori:</label><br>
            <input name="distanciaCole" id="distanciaCole" required="required" type="text">
            <br>
            <label>Contacto:</label><br>
            <input name="telefonoVendedor" id="telefonoVendedor" required="" type="text">
            <br>
            <label>Descripcion:</label><br>
            <textarea name="descripcion" id="descripcion" required="required" type="text"></textarea>
            <br><br>
            <label> Añadir Imagen: </label>
            <br><input name="imagen" id="imagen"  type="file">
            <br><br><br>
            <input name="submit" value="Añadir Piso" type="submit">
        </form>


        <div class="col-lg-9">
            <h4><br> Eliminar Piso <br></h4>
            <form action="adminEliminarPiso.php" method="post" onsubmit="return validacionEliminar()">
                <br><label>Titulo del piso a eliminar: </label>
                <input type="text" name="eliminarPiso" id="eliminarPiso" required="required">
                <input type="submit" name="submit" value="Eliminar">

            </form>
        </div>

        <div> <!--class="col-lg-9" -->

            <table cellpadding="5px" align="auto">
                <tr><h4><br>Listado de Pisos  </h4></tr>
                <tr>
                    <th>Titulo</th>
                    <th>m2</th>
                    <th NOWRAP>Num Habitaciones</th>
                    <th NOWRAP>Planta</th>
                    <th NOWRAP>Precio</th>
                    <th NOWRAP>Distancia</th>
                    <th NOWRAP>Telefono</th>
                    <th NOWRAP>Descripcion</th>
                    <th NOWRAP>Imagenes</th>
                </tr>

				<?php

				include "database.php";
				$tbl_name = "pisos";

				$sql = "Select * From $tbl_name ";
				$res = mysqli_query($mysqli, $sql);

				$i = 0;
				foreach ($res as $piso):

					echo "<tr>";
					echo "<td><a href='adminModificarDatosPiso.php?idP=".$piso['idP']."'>".$piso['titulo']."</a></td>";
					echo "<td NOWRAPs>" . $piso['m2'] . "</td>";
					echo "<td style='padding: 10px;'>" . $piso['nHabit'] . "</td>";
					echo "<td NOWRAP style='padding: 10px;'>" . $piso['planta'] . "</td>";
					echo "<td NOWRAP style='padding: 10px;'>" . $piso['precio'] . "€</td>";
					echo "<td NOWRAP>" . $piso['distanciaCole'] . "km</td>";
					echo "<td NOWRAP>" . $piso['telefonoVendedor'] . "</td>";
					echo "<td>" . $piso['descripcion'] . "</td>";
					echo "<td>" . $piso['imagenes'] . "</td>";
					echo "</tr>";
					$pisos[$i] = $piso;

					$i += 1;
				endforeach;

				//$_SESSION['pisos'] = $pisos;
				//header("Location: adminPisos.php"); ?>

            </table>
        </div>
    </div>
</div>
</div>

</body>

<?php include "footer.php" ?>

</html>
