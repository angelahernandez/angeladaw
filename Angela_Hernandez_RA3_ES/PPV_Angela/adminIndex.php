<?php session_start();

if (!isset($_SESSION['nombreAdmin'])) {
	header('Location: administracion.php');
}

?>

<html>
<head>
    <!-- <meta> <link> -->
	<?php include_once "meta&links.php" ?>

    <title>Admin - Inicio</title>

</head>
<body style="background-color: #FFD7B2;">

<!-- Navigation -->
<?php include "headerAdmin.php"; ?>

<div class="container">
    <form method="post" action="checkregister.php">

        <br>
        <h2>Panel de Administrador</h2>
        <br> <h4>Crear Administrador</h4>

        <label><b>Nombre :</b></label><br>
        <input name="nombreAdmin" required="required" type="text">
        <br>

        <label><b>Contraseña:</b></label><br>
        <input name="contrasenaAdmin" required="required" type="password">
        <br><br>

        <input type="submit" value="Aceptar" name="subtmit">

    </form>

    <h4> Lista de Administradores </h4><br>

    <ol>
		<?php

		include "database.php";
		$tbl_name = "administrador";

		$sql = "Select * From $tbl_name ";
		$res = mysqli_query($mysqli, $sql);

		$i = 0;
		foreach ($res as $admin):
			echo $admin['nombreAdmin'];
			$admins[$i] = $admin;
			echo "<br>";
			$i += 1;

		endforeach;

		$_SESSION['admins'] = $admins;

		?>
    </ol>

</div>
</body>
</html>
