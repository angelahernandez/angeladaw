<?php session_start();

if (!isset($_SESSION['nombreAdmin'])) {
	header('Location: administracion.php');
}

?>

<html>
<head>

    <!-- <meta> <link> -->
	<?php include_once "meta&links.php" ?>

    <title>Admin - Usuarios</title>

    <script>
        <?php
        if (isset($_SESSION['error']) ){
            if($_SESSION['error'] == "eunone"){ ?>
        <?php }
            if($_SESSION['error'] == "eliminaruser") { ?>
        alert("Error al eliminar usuario");
            <?php }} ?>


        function  validacion(){
            var elimiarUser = document.getElementById("eliminarUser").value;

            if(elimiarUser.length == 0){
                alert("Inttroduce un nick de usuario válido");
                return false;
            }
            return true;
        }

    </script>

</head>
<body style="background-color: #FFD7B2;">

<!-- Navigation --> <?php include  "headerAdmin.php";?>

<div class="container">
    <div class="col-lg-9">
        <h4><br> Lista de Usuarios</h4>

		<?php

		include "database.php";
		$tbl_name = "usuario";

		$sql = "Select * From $tbl_name ";
		$res = mysqli_query($mysqli, $sql);
		//$result = mysqli_fetch_array($res);
		//var_dump($res);
        $i = 0;
		foreach ($res as $usuario):

			echo $usuario['nick'];
			$usuarios[$i] = $usuario;
			echo "<br>";
			$i += 1;

		endforeach;

		//$_SESSION['usuarios'] = $usuarios;
		//header("Location: adminIndex.php"); ?>

    </div>

    <div class="col-lg-9">
        <h4><br> Eliminar Usuario</h4>
        <form action="adminEliminarUser.php" method="post" onsubmit="return validacion()">
            <label>Nick del usuario</label>
            <input type="text" name="eliminarUser" id="eliminarUser" required="required">
            <input type="submit" name="submit" value="Eliminar">

        </form>
    </div>
</div>
	<?php include "footer.php"?>
</body>
</html>
