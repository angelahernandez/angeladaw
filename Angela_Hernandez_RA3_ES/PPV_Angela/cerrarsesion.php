<?php

session_start();

if(isset($_SESSION['nombre'])) {
	session_destroy();
	header('Location: index.php');
	die();
}

if(isset($_SESSION['nombreAdmin'])) {
	session_destroy();
	header('Location: administracion.php');
	die();
}

?>