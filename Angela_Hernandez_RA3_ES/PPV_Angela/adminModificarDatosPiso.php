<?php session_start();
if (!isset($_SESSION['nombreAdmin'])) {
	header('Location: administracion.php');
}

$idPiso = $_GET['idP'];


include "database.php";
$tbl_name = "pisos";

$sql = "Select * From $tbl_name Where idP = '$idPiso'";
$res = mysqli_query($mysqli, $sql);


$i = 0;
foreach ($res as $piso):
	$titulo = $piso['titulo'];
	$m2 = $piso['m2'];
	$nHabit = $piso['nHabit'];
	$planta = $piso['planta'];
	$precio = $piso['precio'];
	$distanciaCole = $piso['distanciaCole'];
	$telefonoVendedor = $piso['telefonoVendedor'];
	$descripcion = $piso['descripcion'];

	$pisos[$i] = $piso;

	$i += 1;
endforeach;


?>

<html>
<head>

    <!-- <meta> <link> -->
	<?php include_once "meta&links.php" ?>

    <title>Admin - Modificar</title>

    <script>
        function validacionModifPiso() {

            telefonoVendedorN = document.getElementById("telefonoVendedorN").value;
            if (telefonoVendedorN.length < 9) {
                alert("Introduce telefono del vendedor válido");
                return false;
            }

            return true;
        }

    </script>
</head>
<body style="background-color: #FFD7B2;">

<!-- Navigation -->
<?php include "headerAdmin.php"; ?>


<div class="container">

    <div class="col-lg-3">
        <!--<h4 class="my-4"> <br>Cambiar contraseña</h4> <br>-->
        <h4><br><a href="adminPisos.php">Pisos</a> > Modificar Piso <br></h4>
    </div>

    <div class="col-lg-9">
        <form action="adminModificarPiso.php" method="post" onsubmit="return validacionModifPiso()">
            <table style="td{NOWRAP;}">
                <tr>
                    <th></th>
                    <th>Pisos</th>
                    <th> Modificar Datos Piso</th>
                </tr>
                <tr>
                    <td><br>Titulo</td>
                    <td> <?php echo $titulo; ?></td>
                    <td> <?php echo $titulo; ?></td>
                </tr>
                <tr>
                    <td><br>m2</td>
                    <td> <?php echo $m2; ?></td>
                    <td><input type="text" name="m2N" id="m2N" value="<?php echo $m2; ?>" required="required"></td>
                </tr>
                <tr>
                    <td><br>Numero Habitaciones</td>
                    <td> <?php echo $nHabit; ?> </td>
                    <td><input type="text" name="nHabitN" id="nHabitN" value="<?php echo $nHabit; ?> required="required""></td>
                </tr>
                <tr>
                    <td><br>Planta</td>
                    <td> <?php echo $planta; ?></td>
                    <td><input type="text" name="plantaN" id="plantaN" value="<?php echo $planta; ?>" required="required"></td>
                </tr>
                <tr>
                    <td><br>Precio</td>
                    <td><?php echo $precio; ?></td>
                    <td><input type="text" name="precioN" id="precioN" value="<?php echo $precio; ?>" required="required"></td>
                </tr>
                <tr>
                    <td><br>Distancia al Montessori</td>
                    <td><?php echo $distanciaCole; ?></td>
                    <td><input type="text" name="distanciaColeN" id="distanciaColeN" value="<?php echo $distanciaCole; ?>" required="required"></td>
                </tr>
                <tr>
                    <td><br>Contacto</td>
                    <td><?php echo $telefonoVendedor; ?></td>
                    <td><input type="text" name="telefonoVendedorN" id="telefonoVendedorN" value="<?php echo $telefonoVendedor; ?>"></td>
                </tr>
                <tr>
                    <td><br>Descripcion</td>
                    <td><?php echo $descripcion; ?></td>
                    <td><input type="text" name="descripcionN" id="descripcionN" value="<?php echo $descripcion; ?>" required="required">
                    </td>
                </tr>

                <tr>
                    <td></td>
                    <td><input type="hidden" name="idPiso"  value="<?php echo $idPiso ?>"></td>
                    <td><input type="submit" value="Modificar"></td>
                </tr>

            </table>
        </form>
    </div>
</div>


<?php include "footer.php" ?>

</body>

</html>