<?php include "logo.php"; ?>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
	<div class="container">
        <a class="navbar-brand" href="index.php"><img src="<?php echo $mostrarlogo; ?>" width="100" height="50"></a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
				aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav ml-auto">
                <li><a></a></li>
                <li class="nav-item">
                    <a class="nav-link" href="adminIndex.php"><?php echo $_SESSION['nombreAdmin']; ?></a>
                </li>
				<li class="nav-item">
					<a class="nav-link" href="adminPisos.php">Pisos</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="adminUsuarios.php">Usuarios</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="adminPerfil.php">Perfil</a>
				</li>
                <li class="nav-item">
                    <a class="nav-link" href="adminMensajes.php">Mensajes</a>
                </li>
				<li class="nav-item">
					<a class="nav-link" href="cerrarsesion.php">Cerrar Sesion</a>
				</li>
			</ul>
		</div>
	</div>
</nav>
