<?php session_start();

if (!isset($_SESSION['nick'])) {
	header('Location: index.php');
} else {

	if(isset($_SESSION['tiempo']) ) {

		$inactivo = 300;
		$vida_session = time() - $_SESSION['tiempo'];

		if($vida_session > $inactivo)
		{
			session_unset();
			session_destroy();
			header("Location: index.php");
			exit();
		} else {
			$_SESSION['tiempo'] = time();
		}
	} else {

		$_SESSION['tiempo'] = time();
	}
}

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <!-- <meta> <link>-->
    <?php include "meta&links.php"; ?>

    <title>Favoritos</title>

<body>

<?php include "headerUser.php"; ?>

<!-- Perfil -->
<div class="container">
    <div class="row">
    <div class="col-lg-3">
        <h4><br><a href="userPerfil.php">Perfil</a> > Favoritos <br></h4>
    </div>
    <div class='col-lg-9'></div>
	<?php


	include "database.php";
	$tbl_name1 = "usuario";
	$tbl_name2 = "favoritos";
	$tbl_name3 = "pisos";

	$usuario = $_SESSION['idU'];

	$sql = "Select * From $tbl_name1 inner join $tbl_name2 on $tbl_name1.idU = $tbl_name2.idUsuario
				inner join $tbl_name3 on $tbl_name2.idPiso = $tbl_name3.idP where $tbl_name1.idU = '$usuario'";
	$res = mysqli_query($mysqli, $sql);


	$i = 0;

	//echo "<div class='container'>";
	echo "<div class='row'>";
$direccion = "img/pisos/";
	foreach ($res as $piso):

		$pisos[$i] = $piso;

		echo "<div class='col-lg-4 col-md-6 mb-4'>";
		echo "<div class='card h-100'>";

		echo "<p><img src='".$direccion.$piso['imagenes']."' width='352' height='280'> </p>";
		echo "<div class='card-body'>";
		echo "<h4 class='card-title'>";
		echo "<a href='index.php'>" . $piso['titulo'] . "</a> </h4>";
		$iduser = $_SESSION['idU'];
		$idpiso = $piso['idP'];

		echo "<h5>" . $piso['precio'] . "</h5>";
		echo "<p class='card-text'>Habitaciones: " . $piso['nHabit'] . "km</p>";
		echo "<p class='card-text'>Planta: " . $piso['planta'] . "</p>";
		echo "<p class='card-text'>Distancia: " . $piso['distanciaCole'] . "km</p>";
		echo "<p class='card-text'>Tlfn: " . $piso['telefonoVendedor'] . "</p>";
		echo "<p class='card-text'>Descripcion: " . $piso['descripcion'] . "</p>";

		$fav = "Select * from favoritos where idPiso = '$idpiso' and idUsuario = '$iduser'";
		$favResult = mysqli_query($mysqli, $fav);

		if($favResult->num_rows > 0) {

			echo "<form action='favoritos.php' method='post'>";
			echo "<input type='submit' value='Quitar Fav'>";
			echo "<input type='hidden' name='añadirFav' value='2'>";
			echo "<input type='hidden' name='idusuario' value='".$iduser."'>";
			echo "<input type='hidden' name='idpiso' value='".$idpiso."'>";
			echo "</form>";

		}

		echo "</div></div></div>";

	endforeach;

	echo "</div></div></div>";
	?>
</div>

<?php include "footer.php"; ?>

</body>
</html>