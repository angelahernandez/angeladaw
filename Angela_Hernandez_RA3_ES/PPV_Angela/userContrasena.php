<?php

session_start();
include "database.php";

if (!isset($_SESSION['nick'])) {
	header('Location: userLogin.php');

} else {

	if (isset($_SESSION['tiempo'])) {

		$inactivo = 300;
		$vida_session = time() - $_SESSION['tiempo'];

		if ($vida_session > $inactivo) {
			session_unset();
			session_destroy();
			header("Location: index.php");
			exit();
		} else {
			$_SESSION['tiempo'] = time();
		}
	} else {

		$_SESSION['tiempo'] = time();
	}

}

$tbl_name = "usuario";

$nick = $_SESSION['nick'];
$oldpass = $_POST['oldpassword'];
$newpass = $_POST['newpassword'];
$confirmpass = $_POST['confirmpassword'];


$result = $mysqli->query("SELECT * FROM $tbl_name WHERE nick = '$nick'");
$row = $result->fetch_array(MYSQLI_ASSOC);


if ($row['contrasena'] == $oldpass) {

	if ($newpass == $confirmpass) {
		$sql = "UPDATE  $tbl_name SET contrasena = '$newpass' WHERE nick = '$nick'";
		$actualizar = $mysqli->query($sql);
		echo "actualizada";
		header('Location: userPerfil.php');
	} else {
		header('Location: userPerfilContrasena.php');
	}

} else {

	header('Location: userPerfilContrasena.php');
}
?>
