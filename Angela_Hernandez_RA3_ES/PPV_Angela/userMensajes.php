<?php session_start();


if(!isset($_SESSION['nick'])){
	header ('Location: index.php');
} else {

	if(isset($_SESSION['tiempo']) ) {

		$inactivo = 300;
		$vida_session = time() - $_SESSION['tiempo'];

		if($vida_session > $inactivo)
		{
			session_unset();
			session_destroy();
			header("Location: index.php");
			exit();
		} else {
			$_SESSION['tiempo'] = time();
		}
	} else {

		$_SESSION['tiempo'] = time();
	}

}

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <!-- <meta> <link> -->
	<?php include_once "meta&links.php" ?>
    <title>Mensajes</title>

	<style> td:first-child {font-weight: bold; margin: 10px; padding: 13px;} </style>

</head>
<body>

<!-- Navigation -->
<?php include "headerUser.php"; ?>

<!-- Perfil -->
<div class="container">

	<div class="col-lg-3">
		<h1 class="my-4">Mensajes</h1>

	</div>

	<div>

		<?php
		include "database.php";
		$tbl_name = "mensajeria";
		$idusuario = $_SESSION['idU'];


		$sql = "Select * From $tbl_name  where (visto = 1 and (remitente = '$idusuario' or destinatario = '$idusuario')) or remitente = '$idusuario' ";
		$res = mysqli_query($mysqli, $sql);
        $i = 0;
		foreach ($res as $mensaje):

			echo $mensaje['mensaje'];
			$mensajes[$i] = $mensaje;
			echo "<br>";
			$i += 1;

		endforeach;

		?>

        <h5><br>Mensajes sin leer<br></h5>

		<?php

		$sql2 = "Select * From $tbl_name  where visto = 0  and destinatario = '$idusuario'";
		$res2 = mysqli_query($mysqli, $sql2);

		foreach ($res2 as $mensaje):
			echo $mensaje['mensaje'];
			echo "<br>";

		endforeach;

		if($_GET['leido'] == 1){

			$sql = "UPDATE $tbl_name set visto = 1 where destinatario = '$idusuario'";
			$res = mysqli_query($mysqli, $sql);

		}

		?>

	</div>

	<div>
		<table>
			<tr><th><h4><br><br> Enviar Mensaje a Admin </h4> </th></tr>
			<td>
				<form action="mensajes.php" method="post">
                    <input type="text" name="mensaje" id="mensaje" required="required">
					<input type="submit" name="submit" value="Enviar">
				</form>
			</td>
		</table>
	</div>

</div>

<!-- Footer -->
<?php include "footer.php"; ?>

</body>
</html>