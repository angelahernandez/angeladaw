<?php session_start();
include "database.php";

if (isset($_SESSION['nick'])) {

	if (isset($_SESSION['tiempo'])) {

		$inactivo = 30;
		$vida_session = time() - $_SESSION['tiempo'];

		if ($vida_session > $inactivo) {
			session_unset();
			session_destroy();
			header("Location: index.php");
			exit();
		} else {
			$_SESSION['tiempo'] = time();
		}
	} else {

		$_SESSION['tiempo'] = time();
	}
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- <meta> <link> -->
	<?php include_once "meta&links.php" ?>
    <title>Inicio - PisosPisos</title>

    <script>
		<?php
		if (isset($_SESSION['error']) && $_SESSION['error'] == "rnone") {   ?>
        alert("Registro Correcto, Logeate :D");
		<?php }?>
    </script>

</head>

<body>

<?php

if (isset($_SESSION['nick'])) {
	include "headerUser.php";
} else {
	include "headerSimple.php";
}

?>

<!-- Page Content -->
<div class="container">
    <div class="row">
        <div class="col-lg-3">

            <h1 class="my-4"><br>PisosPisos </br></h1>

        </div> <!-- /.col-lg-3 -->
        <div class="col-lg-9"> </div><!--/.col-lg-9 -->
		<?php include "userMostrarPisos.php"; ?>

    </div>    <!-- /.row -->
</div><!-- /.container -->

<!-- Footer -->
<?php include "footer.php" ?>

</body>
</html>
