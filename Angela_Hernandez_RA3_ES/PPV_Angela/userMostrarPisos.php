<?php

include "database.php";
$tbl_name = "pisos";

$sql = "Select * From $tbl_name";
$res = mysqli_query($mysqli, $sql);
$directorio = "img/pisos/";
$i = 0;
foreach ($res as $piso):

	$pisos[$i] = $piso;

	echo " <div class='col-lg-4 col-md-6 mb-4'>";
	echo "<div class='card h-100'>";
	echo "<p><img src='" . $directorio . $piso['imagenes'] . "' width='352' height='280'> </p>";
	echo "<div class='card-body'>";
	echo "<h4 class='card-title'>";
	echo "<a href='index.php'>" . $piso['titulo'] . "</a> </h4>";

	if (isset($_SESSION['nick'])) {

		if (isset($_SESSION['tiempo'])) {

			$inactivo = 300;
			$vida_session = time() - $_SESSION['tiempo'];

			if ($vida_session > $inactivo) {
				session_unset();
				session_destroy();
				header("Location: index.php");
				exit();
			} else {
				$_SESSION['tiempo'] = time();
			}
		} else {

			$_SESSION['tiempo'] = time();
		}


		$iduser = $_SESSION['idU'];
		$idpiso = $piso['idP'];

		echo "<h5>" . $piso['precio'] . "€</h5>";
		echo "<p class='card-text'>M2:" . $piso['m2'] . "</p>";
		echo "<p class='card-text'>Habitaciones: " . $piso['nHabit'] . "</p>";
		echo "<p class='card-text'>Planta: " . $piso['planta'] . "</p>";
		echo "<p class='card-text'>Distancia: " . $piso['distanciaCole'] . "km</p>";
		echo "<p class='card-text'>Tlfn: " . $piso['telefonoVendedor'] . "</p>";
		echo "<p class='card-text'>Descripcion: " . $piso['descripcion'] . "</p>";
		echo "<a href='pdfcreator.php?idP=" . $idpiso . "'>PDF</a>";
		echo "<br>";


		$fav = "Select * from favoritos where idPiso = '$idpiso' and idUsuario = '$iduser'";
		$favResult = mysqli_query($mysqli, $fav);

		if ($favResult->num_rows == 0) {

			echo "<form action='favoritos.php' method='post'>";
			echo "<input type='submit' value='Favorito'>";
			echo "<input type='hidden' name='añadirFav' value='1'>";
			echo "<input type='hidden' name='idusuario' value='" . $iduser . "'>";
			echo "<input type='hidden' name='idpiso' value='" . $idpiso . "'>";
			echo "</form>";
		}

		if ($favResult->num_rows > 0) {

			echo "<form action='favoritos.php' method='post'>";
			echo "<input type='submit' value='Quitar Fav'>";
			echo "<input type='hidden' name='añadirFav' value='0'>";
			echo "<input type='hidden' name='idusuario' value='" . $iduser . "'>";
			echo "<input type='hidden' name='idpiso' value='" . $idpiso . "'>";
			echo "</form>";

		}
	}

	$_SESSION['pisos'] = $pisos;
	$i += 1;

	echo "</div></div></div>";

endforeach;
?>





