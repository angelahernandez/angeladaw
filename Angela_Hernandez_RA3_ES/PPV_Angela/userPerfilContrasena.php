<?php session_start();

if (!isset($_SESSION['nick'])) {
	header('Location: index.php');

} else {
	if (isset($_SESSION['tiempo'])) {

		$inactivo = 300;
		$vida_session = time() - $_SESSION['tiempo'];

		if ($vida_session > $inactivo) {
			session_unset();
			session_destroy();
			header("Location: index.php");
			exit();
		} else {
			$_SESSION['tiempo'] = time();
		}
	} else {

		$_SESSION['tiempo'] = time();
	}

} ?>

<!DOCTYPE html>
<html lang="en">

<head>

    <!-- <meta> <link> -->
	<?php include "meta&links.php"; ?>
    <title>Cambiar Contraseña</title>

</head>

<body>

<!-- Navigation -->
<?php include "headerUser.php"; ?>

<!-- Perfil -->
<div class="container">

    <div class="col-lg-3">
        <h4><br><a href="userPerfil.php">Perfil</a> > Contraseña <br></h4>
    </div>

    <form action="userContrasena.php" method="post">
        <label>Contraseña Antigua:</label> <br>
        <input type="password" name="oldpassword" id="oldpassword" required="required"> <br><br>
        <label>Nueva contraseña: </label> <br>
        <input type="password" name="newpassword" id="newpassword" required="required"> <br><br>
        <label>Confirmar nueva contraseña: </label> <br>
        <input type="password" name="confirmpassword" id="confirmpassword" required="required"> <br><br>

        <input type="submit" name="submit" value="Cambiar contraseña"> <br><br>

    </form>
</div>

<!-- Footer -->
<?php include "footer.php"; ?>

</body>

</html>
