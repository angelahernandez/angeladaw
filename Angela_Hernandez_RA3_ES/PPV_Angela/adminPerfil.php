<?php session_start();

if (!isset($_SESSION['nombreAdmin'])) {
	header('Location: administracion.php');
}
include "logo.php";
?>

<html>
<head>

    <!-- <meta> <link> -->
	<?php include_once "meta&links.php" ?>
    <title>Admin - Perfil</title>


</head>
<body style="background-color: #FFD7B2;">
<!-- Navigation --> <?php include "headerAdmin.php"; ?>

<div class="container">

    <div class="row">
        <div class="col-lg-9">
            <br> <h4> Perfil</h4> <br>
            <form action="adminLogo.php" method="post" enctype="multipart/form-data">
                <label> Cambiar logo: </label>
                <input name="logo" id="logo"  type="file">
                <input type="submit" name="submit" value="Subir imagen">
            </form>

            <div class="imagen">
                <p><img src="<?php echo $mostrarlogo ?>" width="200" height="150"></p>
            </div>

            <a href="userEliminarCuenta.php" class="list-group-item">Eliminar Cuenta</a>

        </div>
    </div>
</div>

</body>
</html>
