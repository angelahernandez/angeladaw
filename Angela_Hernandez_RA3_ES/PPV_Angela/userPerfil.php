<?php session_start();


if (!isset($_SESSION['nick'])) {
	header('Location: index.php');
} else {

	if (isset($_SESSION['tiempo'])) {

		$inactivo = 300;
		$vida_session = time() - $_SESSION['tiempo'];

		if ($vida_session > $inactivo) {
			session_unset();
			session_destroy();
			header("Location: index.php");
			exit();
		} else {
			$_SESSION['tiempo'] = time();
		}
	} else {

		$_SESSION['tiempo'] = time();
	}

}

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <!-- <META> <LINK> -->
	<?php include "meta&links.php"; ?>
    <title> Perfil </title>

    <style>
        td:first-child {
            font-weight: bold;
            margin: 10px;
            padding: 13px;
        }
    </style>

</head>

<body>

<!-- Navigation -->
<?php include "headerUser.php"; ?>

<!-- Perfil -->
<div class="container">

    <div class="col-lg-3">
        <h1 class="my-4">Perfil</h1>

    </div>
    <div>
        <form action="userModificarDatos.php" method="post">
            <table>
                <tr>
                    <th></th>
                    <th>Perfil</th>
                    <th> Modificar Datos</th>
                </tr>
                <tr>
                    <td>Nick</td>
                    <td> <?php echo $_SESSION['nick']; ?></td>
                    <td> <?php echo $_SESSION['nick']; ?></td>
                </tr>
                <tr>
                    <td>Nombre</td>
                    <td> <?php echo $_SESSION['nombre']; ?> </td>
                    <td><input type="text" name="newnombre" value="<?php echo $_SESSION['nombre']; ?>"></td>
                </tr>
                <tr>
                    <td NOWRAP>Primer Apellido</td>
                    <td><?php echo $_SESSION['apellido1']; ?></td>
                    <td><input type="text" name="newapellido1" value="<?php echo $_SESSION['apellido1']; ?>"></td>
                </tr>
                <tr>
                    <td NOWRAP>Segundo Apellido</td>
                    <td><?php echo $_SESSION['apellido2']; ?></td>
                    <td><input type="text" name="newapellido2" value="<?php echo $_SESSION['apellido2']; ?>"></td>
                </tr>
                <tr>
                    <td>Correo</td>
                    <td><?php echo $_SESSION['correo']; ?></td>
                    <td><input type="text" name="newcorreo" value="<?php echo $_SESSION['correo']; ?>"></td>
                </tr>
                <tr>
                    <td>Edad</td>
                    <td><?php echo $_SESSION['edad']; ?></td>
                    <td><input type="text" name="newedad" value="<?php echo $_SESSION['edad']; ?>"></td>
                </tr>
                <tr>
                    <td>Teléfono</td>
                    <td><?php echo $_SESSION['telefono']; ?></td>
                    <td><input type="text" name="newtelefono" value="<?php echo $_SESSION['telefono']; ?>"></td>
                </tr>
                <tr>
                    <td>Contraseña</td>
                    <td><?php echo "-" ?></td>
                    <td>
                        <a href="userPerfilContrasena.php" name="Cambiar Contraseña">Cambiar Contraseña</a>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td><input type="submit" value="Modificar" onsubmit="return validacion()"></td>
                </tr>

            </table>

        </form>

        <div class="list-group">
            <p></p>
            <a href="userFavoritos.php" class="list-group-item">Pisos en Favoritos</a>
        </div> <!-- list group -->


        <div class="list-group">
            <p></p>
            <a href="userEliminarCuenta.php" class="list-group-item">Eliminar Cuenta</a>
        </div> <!-- list group -->
    </div>


</div><!-- container -->

<!-- Footer -->
<?php include "footer.php"; ?>

</body>
</html>