<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Meta & Link -->
    <?php include "meta&links.php"; ?>

    <title>Registro</title>

    <script>
		<?php session_start();
		if(isset($_SESSION['error']) && $_SESSION['error'] = "register"){ ?>
            alert("Ha habido un error en el Registro, prueba con otro nick");
		<?php } ?>

        function validacion() {

            telefono = document.getElementById("telefono").value;
            if (telefono.length < 9) {
                alert("Introduce un teléfono válido");
                return false;
            }
            return true;
        }
    </script>
</head>
<body>

<!-- Navigation -->
<?php include "headerSimple.php"; ?>

<!-- Register -->

<div style="margin-left: 120px">

    <h2><br> Registro <br></h2>
    <form name="register" action="checkregister.php" method="post" onsubmit="return validacion()">

        <label>Nick: </label><br>
        <input type="text" name="nick" id="nick" placeholder="Nick: " required="required"><br><br>

        <label>Nombre: </label><br>
        <input type="text" name="nombre" id="nombre" placeholder="Nombre: " required="required"><br><br>

        <label>Primer Apellido: </label><br>
        <input type="text" name="apellido1" id="apellido1" placeholder="Primer Apellido: " required="required"><br><br>

        <label>Segundo Apellido: </label><br>
        <input type="text" name="apellido2" id="apellido2" placeholder="Segundo Apellido: " required="required"><br><br>

        <label>Correo:</label><br>
        <input type="email" name="correo" id="correo" placeholder="Correo: " required="required"><br><br>

        <label>Contrasena (min 6 caracteres):</label><br>
        <input type="password" name="contrasena" id="contrasena" placeholder="Contrasena: " required="required"><br><br>

        <label>Edad: </label><br>
        <input type="text" name="edad" id="edad" placeholder="Edad: " required="required"><br><br>

        <label>Telefono: </label><br>
        <input type="text" name="telefono" id="telefono" placeholder="Telefono: "><br>

        <input type="submit" name="submit" value="Enviar">
        <br><br>

    </form>
</div>

<!-- Footer -->
<?php include "footer.php"; ?>
</body>
</html>