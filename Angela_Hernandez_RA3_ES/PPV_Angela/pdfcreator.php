
<?php

$idpiso = $_GET['idP'];

include "database.php";
$tbl_name = "pisos";

$sql = "SELECT * FROM $tbl_name WHERE idP = $idpiso";
$res = mysqli_query($mysqli, $sql);

foreach ($res as $datos):

	$titulo = $datos['titulo'];
	$m2 = $datos['m2'];
	$nHabit = $datos['nHabit'];
	$planta = $datos['planta'];
	$precio = $datos['precio'];
	$distanciaCole = $datos['distanciaCole'];
	$telefonoVendedor = $datos['telefonoVendedor'];
	$descripcion = $datos['descripcion'];
	$imagenes = $datos['imagenes'];

endforeach;


ob_start();
require('fpdf/fpdf.php');

$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetFont('Arial', 'B', 30);
$pdf->Cell(40, 10, "Datos del piso");
$pdf->Ln(); $pdf->Ln();
$pdf->SetFont('Arial', 'B', 15);
$pdf->Write(20, "Nombre: " . utf8_decode($titulo));
$pdf->Ln();
$pdf->Write(20, "M2: " . utf8_decode($m2));
$pdf->Ln();
$pdf->Write(20, "Habitaciones: " . utf8_decode($nHabit));
$pdf->Ln();
$pdf->Write(20,  "Planta: " . utf8_encode($planta));
$pdf->Ln();
$pdf->Write(20,  "Precio: " . utf8_encode($precio) . " euros");
$pdf->Ln();
$pdf->Write(20,  "Distancia al Colegio Montesorri: " . utf8_encode($distanciaCole) . " KM");
$pdf->Ln();
$pdf->Write(20,  "Tlfn Contacto: " . utf8_encode($telefonoVendedor));
$pdf->Ln();
$pdf->Write(20, "Descripcion: " . utf8_encode($descripcion));
$pdf->Ln();
$pdf->AddPage();
$pdf->Ln();
$pdf->Write(1,  "Imagen: ");
$pdf->Image('img/pisos/'.$imagenes, 20, 50, 120, 60);
$pdf->Ln(); $pdf->Ln();
$pdf->Output();

?>
