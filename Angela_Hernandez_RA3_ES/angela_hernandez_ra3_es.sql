-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-11-2020 a las 16:34:15
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `angela_hernandez_ra3_es`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `idA` int(11) NOT NULL,
  `nombreAdmin` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `contrasenaAdmin` varchar(50) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`idA`, `nombreAdmin`, `contrasenaAdmin`) VALUES
(9, 'admin', 'admin'),
(10, 'asd', 'asd'),
(12, 'vicente', 'vicente'),
(14, 'pepe', 'pepe'),
(15, '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `favoritos`
--

CREATE TABLE `favoritos` (
  `idUsuario` int(11) NOT NULL,
  `idPiso` int(11) NOT NULL,
  `fav` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `favoritos`
--

INSERT INTO `favoritos` (`idUsuario`, `idPiso`, `fav`) VALUES
(19, 27, 1),
(19, 35, 1),
(19, 39, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logo`
--

CREATE TABLE `logo` (
  `id` int(11) NOT NULL,
  `logo` longblob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `logo`
--

INSERT INTO `logo` (`id`, `logo`) VALUES
(1, 0x6c6f676f312e706e67);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensajeria`
--

CREATE TABLE `mensajeria` (
  `idM` int(11) NOT NULL,
  `remitente` int(11) NOT NULL,
  `mensaje` varchar(500) COLLATE utf8_spanish2_ci NOT NULL,
  `destinatario` int(11) NOT NULL,
  `visto` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `mensajeria`
--

INSERT INTO `mensajeria` (`idM`, `remitente`, `mensaje`, `destinatario`, `visto`) VALUES
(1, 19, 'Hola admin, me interesa la Mansion Gaming', -1, 1),
(2, -1, 'Perfecto, eres el primer cliente que lo solicita', 19, 0),
(3, 20, 'Hola admin, soy Vicente y le voy a poner un 10 a Angela', -1, 1),
(4, 20, 'Me parece correcto Vicente, yo también lo haría', -1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pisos`
--

CREATE TABLE `pisos` (
  `idP` int(11) NOT NULL,
  `titulo` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `nHabit` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `precio` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `distanciaCole` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `telefonoVendedor` varchar(9) COLLATE utf8_spanish2_ci NOT NULL,
  `descripcion` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `imagenes` varchar(500) COLLATE utf8_spanish2_ci NOT NULL,
  `m2` varchar(3) COLLATE utf8_spanish2_ci NOT NULL,
  `planta` varchar(3) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `pisos`
--

INSERT INTO `pisos` (`idP`, `titulo`, `nHabit`, `precio`, `distanciaCole`, `telefonoVendedor`, `descripcion`, `imagenes`, `m2`, `planta`) VALUES
(27, 'Casa Moderna', '3 dormitorios, 2 baños, cocina, salón', '460.000', '200', '654781230', 'Alejado de la ciudad, perfecto para desconectar', 'casa_jardin_piscina.jpg', '120', '2'),
(28, 'Piso Luminoso', '3 dorm, 1 baño, cocina, salón', '298.000', '30', '698547112', 'Piso bastante luminoso, tiene ascensor y terraza amplia', '1.PNG', '80', '4ª'),
(29, 'Cabaña en el árbol', '1 dormitorio, 1 baño, salon-cocina', '70.600', '197', '693258741', 'Cabaña para disfrutar de la naturaleza', 'cabana_arbol.jpg', '47', '2'),
(30, 'Piso Azulado', '2 dormitorios, 1 baño, cocina, salón', '90.800', '24', '630301055', 'Piso moderno con muebles azulados, tiene ascensor', 'azulado.PNG', '68', '5'),
(31, 'Bunker', '2 dormitorios, 1 baño, cocina y salón', '120.500', '150', '695832440', 'Casa que no es casa, es un BUNKER!! ', 'casa_bunker.jpg', '55', '2'),
(32, 'Piso sencillo pero bonito', '2 dormitorios, 1 baños, cocina-salón', '75.000', '31', '665214332', 'Cocina tiene electrodomésticos básicos. Decoración simple pero bonita. Tiene ascensor', 'piso_blanco.jpg', '68', '4'),
(33, 'Casa Faro', '4 dormitorios, 2 baños, cocina, salón, sala de estar', '240.000', '240', '657357351', 'Casa conectada con Faro. Preciosas vistas al mar', 'casaFaro.jpg', '200', '3'),
(34, 'Sever Led Asac', '3 dormitorios, 1 baño, cocina, salón', '265.000', '317,4', '644112780', 'Casa empezada por el tejado', 'patas_arriba.jpg', '182', '2'),
(35, 'Mansion Gaming', '5 dormitorios, 2 baños, cocina, salón', '450.000', '410', '666999666', 'Decoracion Led por todos los lados. En un futuro, tendrá piscina', 'mansionGaming.jpg', '200', '2'),
(36, 'Pisito de Chill', '3 dormitorios, 2 baños, salón para ver Netflix, cocina', '92.000', '33', '633147852', 'Maravilloso sofá para ver Netflix, alfombras suavecitas incluidas', 'alfombras_suaves.jpg', '78', '8'),
(37, 'Tan dura como una Piedra', '2 dormitorios, 1 baño, cocina y salón', '120.000', '521', '698740021', 'Igual que las casas de pueblo. Con hoguera para los embutidos y las chuletas', 'casa_de_Piedra.jpg', '88', '2'),
(38, 'Area51', '2 habitaciones, 1 baño', '350.000', '650', '654789000', 'La casa donde viven los extraterrestres y nos controlan. No tiene cocina porque no comen.', 'ovni.jpg', '93', '1'),
(39, 'Palacio sobre agua', '4 dormitorios, 2 baños, cocina, salón', '2.500.000', '670', '987654321', 'Te encantaría vivir aqui pero no te lo puedes permitir :)\r\nY si puedes, contrata a alguien para que te limpie', 'palaciosobreagua.jpg', '230', '2'),
(40, 'Piso de Soltera', '2 habitaciones, 1 baño, salón, cocina', '66.000', '60', '600333157', 'Pisito normalito para solteras (o no)', 'casa_sencilla.jpg', '80', '3'),
(41, 'Piso Completo', '1 habitación, 1 cuarto de estar, 1 baño, cocina, salón ', '156.000', '120', '610240357', 'Piso con un dormitorio, zona para trabajar y lo básico para sobrevivir', 'pisoCompacto.jpg', '124', '2'),
(42, 'Pisito Espiritual', '2 dormitorios, 1 baño, cocina grande conectada con salón', '99.000', '220', '630798412', 'Perfecto para limpiar tu aura', 'pisoEspiritual.jpg', '84', '3'),
(43, 'Pisito sencillito con lo esencial', '2 dormitorios, 1 baño, cocina y salón', '55.000', '18', '678451204', 'Piso con muebles básicos', 'tonos_grises.jpg', '65', '2'),
(44, 'Casacuario', '2 dormitorios, 2 baños, cocina y salón', '250.000', '541', '632587419', 'El acuario en casa', 'acuario.jpg', '75', '1'),
(45, 'Bajo El Mar', '3 habitaciones, 1 baño, cocina y salón', '450.000', '865', '688125748', 'Pasar momento tan románticos como la Sirenita y el Príncipe Eric ', 'bajoElMar.jpg', '87', '1'),
(46, 'Rollito de Primavera', '1 dormitorio', '20.000', '1203', '621789354', 'Solo es una habitación, pero si te vas con una caravana, debe ser precioso', 'rollito.jpg', '30', '1'),
(47, 'Piso Saludable', '2 dormitorios, 1 baño, cocina y salón', '75.000', '70', '632985190', 'Principales colores: Blanco y Verde (perfecto para veganos)', 'pisoParaVeganos.jpg', '70', '2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idU` int(11) NOT NULL,
  `nick` varchar(25) COLLATE utf8_spanish2_ci NOT NULL,
  `nombre` varchar(25) COLLATE utf8_spanish2_ci NOT NULL,
  `apellido1` varchar(25) COLLATE utf8_spanish2_ci NOT NULL,
  `apellido2` varchar(25) COLLATE utf8_spanish2_ci NOT NULL,
  `edad` varchar(25) COLLATE utf8_spanish2_ci NOT NULL,
  `correo` varchar(25) COLLATE utf8_spanish2_ci NOT NULL,
  `contrasena` varchar(25) COLLATE utf8_spanish2_ci NOT NULL,
  `telefono` varchar(9) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idU`, `nick`, `nombre`, `apellido1`, `apellido2`, `edad`, `correo`, `contrasena`, `telefono`) VALUES
(4, 'pepe', 'pepe', 'pepito', 'pepitin', '21', 'pepe@gmail.com', 'pepe', '630520410'),
(14, 'monti', 'Jorge', 'Montalbán', 'Pedra', '21', 'jorgemon@gmail.com', 'jorgenitales', '633718394'),
(18, 'carmenDM', 'Carmen', 'De Mairena', 'Y nada mas', '999', 'cmairena@gmail.com', 'carmendemairena', '654987120'),
(19, 'angela', 'Angela', 'Hernandez', 'Poyatos', '20', 'angela@montessori.com', 'angelaa', '659481232'),
(20, 'vicentePonmeUn10', 'Vicente', 'ElMejor', 'SinDuda', '25', 'vicente@eresun.crack', 'vicente', '654321987');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`idA`);

--
-- Indices de la tabla `favoritos`
--
ALTER TABLE `favoritos`
  ADD PRIMARY KEY (`idUsuario`,`idPiso`),
  ADD KEY `idPiso` (`idPiso`);

--
-- Indices de la tabla `logo`
--
ALTER TABLE `logo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mensajeria`
--
ALTER TABLE `mensajeria`
  ADD PRIMARY KEY (`idM`);

--
-- Indices de la tabla `pisos`
--
ALTER TABLE `pisos`
  ADD PRIMARY KEY (`idP`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idU`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administrador`
--
ALTER TABLE `administrador`
  MODIFY `idA` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `logo`
--
ALTER TABLE `logo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `mensajeria`
--
ALTER TABLE `mensajeria`
  MODIFY `idM` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `pisos`
--
ALTER TABLE `pisos`
  MODIFY `idP` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idU` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `favoritos`
--
ALTER TABLE `favoritos`
  ADD CONSTRAINT `favoritos_ibfk_1` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idU`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `favoritos_ibfk_2` FOREIGN KEY (`idPiso`) REFERENCES `pisos` (`idP`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
